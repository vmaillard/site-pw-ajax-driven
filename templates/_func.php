<?php


function isCurrent($page_to_test) {
	if (wire('page')->id == $page_to_test->id) {
		return "current";
	}
}

