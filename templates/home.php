<?php 
/**
 * $config->ajax est un boolean qui teste la requête ajax
 * if($config->ajax) : s'il y a une requête ajax
 * if(!$config->ajax) : s'il n'y a pas de requête ajax
*/
?>

<?php if(!$config->ajax): // if not ajax ?>

<?php include("include/head.php"); ?>

<?php include("include/menu.php"); ?>



<main id="main">
	<div class="current_content">
<?php endif; // end if not ajax ?>
	
	<p>
		<?=$page->title?>
	</p>

<?php if(!$config->ajax): // if not ajax ?>
	</div>
</main>



<?php include("include/foot.php"); ?>

<?php endif; // end if not ajax ?>