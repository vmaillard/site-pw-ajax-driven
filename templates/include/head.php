<!DOCTYPE html>
<html lang="fr">

<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1, maximum-scale=1">

	<title><?=$page->title?></title>

	<link rel="stylesheet" href="<?=$config->urls->templates?>/styles/main.css" />

	<script src="<?=$config->urls->templates?>scripts/main.js" defer></script>

</head>

<body>