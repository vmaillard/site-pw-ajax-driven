<?php 
/**
 * Pour le bon fonctionnement d'ajax :
 * une class "ajax-link"
 * un attribut "href"
 * un attribut "name"
 */
?>
<header>
	<nav>
		<h1>
			<a name="<?=$homepage->title?>" class="ajax-link <?=isCurrent($homepage)?>" href="<?=$homepage->url?>"><?=$homepage->title?></a>
		</h1>
<?php foreach ($homepage_children as $child): ?>
		<a name="<?=$child->title?>" class="ajax-link <?=isCurrent($child)?>" href="<?=$child->url?>"><?=$child->title?></a>
<?php endforeach ?>
	</nav>
</header>