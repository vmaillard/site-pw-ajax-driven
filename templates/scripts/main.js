
document.addEventListener("DOMContentLoaded", function() {
	init();
}, false);


function init() {
	ajax_interactions();

	// toggleHistoryVisitNum = 0;
}

function ajax_interactions() {
	var ajax_links = document.querySelectorAll('.ajax-link'); 
	ajax_links.forEach(function (item) {
		item.addEventListener('click', clickClassAjax);
	});
}

function clickClassAjax() {
	event.preventDefault();

	if (this.classList.contains('current')) {
        return true; // arrêt fonction
    }

    toggleCurrent(this);
	toggleTabTitle(this.getAttribute("name"));

    if (this.hasAttribute("href")) {
		var url = this.getAttribute("href");

		ajaxCall(url);
		history.pushState('', +url, url); // ajout de l'url à l'historique

	} else {
		console.log("Aucun href.");
	}
	
}


function ajaxCall(url) {
	var xhttp = new XMLHttpRequest();
	xhttp.open("GET", url, true);
	xhttp.setRequestHeader﻿("X-Requested-With", "XMLHttpRequest");

	xhttp.onreadystatechange = function() {
	  	xhttp.getAllResponseHeaders();﻿
	    if (this.readyState == 4 && this.status == 200) {
	    	insertAjaxContent(this.responseText);

	    	// if(!toggleHistoryVisitNum) {
	        	toggleHistory();
	      	// }

			toggleHistoryVisitNum++;
	    }
	};
	
	xhttp.send();
}

function insertAjaxContent(data) {
	var div = document.createElement("DIV");
	div.insertAdjacentHTML('beforeend', data);

	var current_content = document.querySelector(".current_content");
	current_content.parentNode.removeChild(current_content);

	var main = document.querySelector("main");
	div.classList.add("current_content");
	main.appendChild(div);
}
	

function toggleTabTitle(str) {
	document.title = str;
}

function toggleCurrent(el) {
	var current_link = document.querySelector(".current");
	if (current_link) current_link.classList.remove("current");
    el.classList.add("current"); // nouveau .current
}

function toggleHistory(){
    window.onpopstate = function(event) {
    if (window.location.hash == ""){
    	var el = returnElementFromPath(location.pathname);
    	toggleCurrent(el);
    	toggleTabTitle(el.getAttribute("name"));
    	ajaxCall(location.pathname);
    }
    };
}

function returnElementFromPath(path) {
	var ajax_links = document.querySelectorAll('.ajax-link');
	for (var i = 0; i <= ajax_links.length; i++) {
		if (path == ajax_links[i].getAttribute("href")) {
          return ajax_links[i];
          break;
		}
	}
}












